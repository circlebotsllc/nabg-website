# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

 This is a Wordpress site for NABG

### How do I get set up? ###

* Create Vagrant Box
* changed the host port from 8080 - 8081
* cd /var/www
* ln -s /vagrant/data/wordpress wordpress
* Create Mysql nabg database
* chmod -R 777 wordpress/ = locally
* a2moden rewrite
* Made changes to /etc/apache2/sites-enabled/000-default.conf
* Updated File Path from /var/www/html to /var/www/wordpress
* Added Document tag
* <Directory "/var/www/html/wordpress">
        AllowOverride All
  </Directory>
* sudo service apache2 restart